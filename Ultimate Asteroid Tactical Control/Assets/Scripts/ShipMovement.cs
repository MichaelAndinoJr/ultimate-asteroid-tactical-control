﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{ 
    // Create a variable for our transform component
    private Transform tf; // variable for transform component
    public float TurnSpeed; // variable to adjust turning speed of character
    public float Speed; //variable to adjust moving speed
                        
                        
    void Start()
    {
        // Load our transform component into our variable
        tf = GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {
        

        if (Input.GetButton("left"))
        {

            tf.Rotate(0, 0, TurnSpeed * Time.deltaTime);

        }
        if (Input.GetButton("right"))
        {

            tf.Rotate(0, 0, -TurnSpeed * Time.deltaTime);
        }
        if (Input.GetButton("forward"))
        {
           
            tf.position += tf.up * Speed * Time.deltaTime;
        }
        if (Input.GetButton("Reverse"))
        {
            tf.position += -tf.up * Speed * Time.deltaTime;
        }
    }
        
        
          
        
        
       



   
}