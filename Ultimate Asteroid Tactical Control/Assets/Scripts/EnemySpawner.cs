﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour

{
    public List<GameObject> enemies;
    public List<Transform> spawnPoints;
    public List<GameObject> asteroidtypes;
    public int maxEnemies = 3; // max amount of enemies allowed on screen
    public int enemiesSpawned = 0;
    public float respawnRate = 2f; // how often a spawn will appear since previous spawn
    EnemySpawner nextEnemy;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
     void Update()
    {
            GameObject nextEnemy = enemies[Random.Range(0, enemies.Count)];
            Transform spawns = spawnPoints[Random.Range(0, spawnPoints.Count)];
        
        
            for (int i = 0; i < enemies.Count; i++) // Condition check to iterate through the enemy array
            {
                nextEnemy = enemies[Random.Range(0, enemies.Count)];
            }
            if (enemiesSpawned < maxEnemies) // Checks if the max amount of enemies has spawned
            {
            // Instantiates a new enemy spawn from the enemy spawner defined prefabs and positions it at a random spawn point based on the length of the spawn point array
            GameObject enemy = Instantiate(nextEnemy, spawns.position, spawns.rotation);
            enemiesSpawned++; // Increments the current spawn count on the playfield
            }
        

    }
   
}
