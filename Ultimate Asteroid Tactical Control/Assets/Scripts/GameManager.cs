﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    Vector3 playerstart; // variable to set player start at center
    //Lists
    //public List<GameObject> enemies;
   // public List<Transform> spawnPoints;
   // public List<GameObject> asteroidtypes;
    // Values
    public int score = 0; // Keeps track of the current score
    public int asteroidWorth; // variable that sets worth of asteroid
    public int enemyShipWorth; // variable that sets worth of enemy ship

    //Health
    public int playerHealth; 
    public int asteroidHealth;
    public int enemyHealth;

    //Damages
    public GameObject outofBounds; //game object that will destroy objects
    public int enemyDamage; // how much enemy will take away from player health on collision
    public int LaserDamage; // how much damage is done on enemy collision
    
    //player    
    public GameObject player;  // stores the gameobject for player
    public int lives; // This defines the max amount of player lives
    public int currentPlayerHealth; // This will be initialized by the total player health at start

    //Laser
    public GameObject laser; // Variable to store the laser game object
    public float lasertimelength; // This will determine the length of time that a laser will remain before it is destroyed 
    public float laserSpeed; // This sets the speed of the laser

    // Enemy
    public float enemyShipSpeed = 2f; // Allows designers to adjust the enemy ships speed
    public float enemyShipRotation = 2f; // Allows designers to adjust the enemy ships rotation


    // Start is called before the first frame update
    void Start()
    {
        playerstart = new Vector3(0, 0, 0); // center of map is 0,0,0
        player.transform.position = playerstart; //sets the player to the center of the map
    }

    // Update is called once per frame
    void Update()
    {
        //quit game using escape
        if (lives <= 0) // checks to see if lives have reached 0. Game will quit at 0 lives
        {

            Application.Quit();
        }

    }
    void Awake ()
    {
        if (instance == null)
        {
            instance = this; // Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);

        }
    }
}
